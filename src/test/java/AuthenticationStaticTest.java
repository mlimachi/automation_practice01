import authenticationStatic.Authentication;
import authenticationStatic.CredentialsStaticService;
import authenticationStatic.PermissionStaticService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

public class AuthenticationStaticTest {

    @Test
    public void verifyAuthenticationMock() {
        // PASO 2. utilizar un objeto mockeado
        MockedStatic<CredentialsStaticService> credentialsMocked = Mockito.mockStatic(CredentialsStaticService.class);
        MockedStatic<PermissionStaticService> permissionMocked = Mockito.mockStatic(PermissionStaticService.class);

        // PASO 3. configurar cómo se comporta nuestro objeto falso
        credentialsMocked.when(()->CredentialsStaticService.isValidCredential("admin", "111")).thenReturn(true);
        credentialsMocked.when(()->CredentialsStaticService.isValidCredential("juan", "123")).thenReturn(true);
        credentialsMocked.when(()->CredentialsStaticService.isValidCredential("rosa", "456")).thenReturn(true);

        permissionMocked.when(()->PermissionStaticService.getPermission("admin")).thenReturn("CRUD");
        permissionMocked.when(()->PermissionStaticService.getPermission("juan")).thenReturn("CR");
        permissionMocked.when(()->PermissionStaticService.getPermission("rosa")).thenReturn("R");

        // PASO 4. utilizar el objeto falso: mock
        Authentication authentication = new Authentication();

        // hacer el test, para los 3 usuarios y 1 no autorizado
        String expectedResult = "user authenticated successfully with permission: [CRUD]";
        String actualResult = authentication.login("admin", "111");
        Assertions.assertEquals(expectedResult, actualResult, "ERROR. Las credenciales no son correctas");

        expectedResult = "user authenticated successfully with permission: [CR]";
        actualResult = authentication.login("juan", "123");
        Assertions.assertEquals(expectedResult, actualResult, "ERROR. Las credenciales no son correctas");

        expectedResult = "user authenticated successfully with permission: [R]";
        actualResult = authentication.login("rosa", "456");
        Assertions.assertEquals(expectedResult, actualResult, "ERROR. Las credenciales no son correctas");

        expectedResult = "user or password incorrect";
        actualResult = authentication.login("usuario_extrano", "999999999");
        Assertions.assertEquals(expectedResult, actualResult, "ERROR. Las credenciales no son correctas");

        // cerrar objetos, para poder usarlo varias veces
        credentialsMocked.close();
        permissionMocked.close();
    }
}
