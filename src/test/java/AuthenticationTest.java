import authentication.Authentication;
import authentication.CredentialsService;
import authentication.PermissionService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class AuthenticationTest {
    // PASO 2. crear un objeto mock, vacio
    CredentialsService credentialsServiceMock = Mockito.mock(CredentialsService.class);
    PermissionService permissionServiceMock = Mockito.mock(PermissionService.class);

    @Test
    public void verifyAuthenticationMock() {
        // PASO 3. configurar cómo se comporta nuestro objeto falso
        Mockito.when(credentialsServiceMock.isValidCredential("admin","111")).thenReturn(true);
        Mockito.when(credentialsServiceMock.isValidCredential("juan","123")).thenReturn(true);
        Mockito.when(credentialsServiceMock.isValidCredential("rosa","456")).thenReturn(true);
        Mockito.when(permissionServiceMock.getPermission("admin")).thenReturn("CRUD");
        Mockito.when(permissionServiceMock.getPermission("juan")).thenReturn("CR");
        Mockito.when(permissionServiceMock.getPermission("rosa")).thenReturn("R");

        // PASO 4. utilizar el objeto falso: mock
        Authentication authentication = new Authentication();
        authentication.setCredentialsService(credentialsServiceMock);
        authentication.setPermissionService(permissionServiceMock);

        // hacer el test, para los 3 usuarios y 1 no autorizado
        String expectedResult = "user authenticated successfully with permission: [CRUD]";
        String actualResult = authentication.login("admin", "111");
        Assertions.assertEquals(expectedResult, actualResult, "ERROR. Las credenciales no son correctas");

        expectedResult = "user authenticated successfully with permission: [CR]";
        actualResult = authentication.login("juan", "123");
        Assertions.assertEquals(expectedResult, actualResult, "ERROR. Las credenciales no son correctas");

        expectedResult = "user authenticated successfully with permission: [R]";
        actualResult = authentication.login("rosa", "456");
        Assertions.assertEquals(expectedResult, actualResult, "ERROR. Las credenciales no son correctas");

        expectedResult = "user or password incorrect";
        actualResult = authentication.login("usuario_extrano", "999999999");
        Assertions.assertEquals(expectedResult, actualResult, "ERROR. Las credenciales no son correctas");

        // PASO 5. garantizar que utilizamos el objeto mock, se necesita verificarlo
        Mockito.verify(credentialsServiceMock).isValidCredential("admin", "111");
        Mockito.verify(credentialsServiceMock).isValidCredential("juan", "123");
        Mockito.verify(credentialsServiceMock).isValidCredential("rosa", "456");
        Mockito.verify(permissionServiceMock).getPermission("admin");
        Mockito.verify(permissionServiceMock).getPermission("juan");
        Mockito.verify(permissionServiceMock).getPermission("rosa");
    }
}
